import logo from './logo.svg';
import './App.css';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { Login } from './Components/Login';
import {Routes, Route} from 'react-router-dom'
import { Home } from './Components/Home'
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getUser } from './Redux/Actions/actions';


function App() {
  
  const dispatch =  useDispatch()
  useEffect(() => {
    dispatch(getUser())
  },[])
  return (
    <div className='homepage'>
    
    <Routes>
      <Route exact path='/' element= {<Login/>}></Route>
      <Route exact path='/home' element= {<Home/>}></Route>
    </Routes>
    </div>
  );
}

export default App;
