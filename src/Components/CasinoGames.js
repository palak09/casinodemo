import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useEffect, useState } from "react";
import {playGame} from '../Redux/Actions/casinoAction'

export const CasinoGames = (props) => {

  const [count, setCount] = useState(21)
  const dispatch =useDispatch()
  const Casinogames = useSelector((state) => state.casinoReducer.games);
  const users = useSelector(state => state.loginReducer.users)
  let [displayCategory, setDisplay] = useState(Casinogames.filter(
    (games) => games.game_category == "Slots"
  ))

  useEffect(() => {
     setDisplay(Casinogames.filter(
      (games) => games.game_category == props.selectedCategory
     ))
     console.log(props.selectedCategory)
  },[props.selectedCategory])
  
  const loadMore = () => {
  setCount(count+21)
  }

  const games = (game) => {
    return (
      <>
        <div className="games">
        <img
            src={game.game_image}
            onClick = {() => dispatch(playGame( game.game_id ,users.data[0].casino_account_id))}
            class="card-img-top"
            alt="cannot display"
            style={{ width: "150px", height: "150px", margin :"10px" }}
          />
          <div className="game-description">{game.game_name}</div>
          </div>
        </>
    );
  };
  // display = games.filter(game => game.game_category==props.selectedCategory)

  // const InstantWin = state.filter(
  //   (games) => games.game_category == "Instant Win"
  // );
  // const Slots = state.filter((games) => games.game_category == "Slots");
  // const WinOrCrash = state.filter(
  //   (games) => games.game_category == "Win or Crash"
  // );
  // const TableAndCards = state.filter(
  //   (games) => games.game_category == "Table & Cards"
  // );
  // const VideoPoker = state.filter(
  //   (games) => games.game_category == "Video Poker"
  // );
  // const Lottery = state.filter((games) => games.game_category == "Lottery");

  return (
    <>
      
      <div className="d-flex flex-row flex-wrap justify-content-center h-100">
        {displayCategory.slice(0, count).map(games)}
      </div>
      <div className="text-center">
      <button className="btn my-3" onClick={loadMore}> Show more results</button>
      </div>
      
    </>
  );
};
