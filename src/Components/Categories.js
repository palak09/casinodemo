import { useSelector } from "react-redux";
import Slider from "react-slick";
import { Home } from "./Home";

const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 2,
    
  };


export const Categories = (props) => {
    const games = useSelector(state => state.casinoReducer.games)
    let category = []
    let display = []
    
    const categoryButton = (cat) => {
        return (
            <button onClick={() =>props.setSelectedCategory(cat)} className="category"> {cat}</button>
        )
    }


   {games.forEach(game => {
             !(category.includes(game.game_category)) &&
                 category.push(game.game_category)
             }
        )}
       
        
           

          return (
            <div>
  <Slider {...settings}>
                 {category.map(categoryButton)}
         </Slider>
            </div>
          
  );

       
}
