import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logout } from "../Redux/Actions/actions";
import { fetchCasino } from "../Redux/Actions/casinoAction";
import { CasinoGames } from "./CasinoGames";
import { Categories } from "./Categories";

export const Home = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const token = useSelector((state) => state.loginReducer.token);
  const isLoading = useSelector((state) => state.casinoReducer.isLoading);
  const users = useSelector((state) => state.loginReducer.users);

  const [selectedCategory, setSelectedCategory] = useState("Slots");
  useEffect(() => {
    !token && navigate("/");
  }, [token]);

  useEffect(() => {
    console.log(isLoading);

    dispatch(fetchCasino());
  }, []);

  const handleClick = () => {
    dispatch(logout());
  };
  console.log(users);

  return (
    <>
      <div>
        <nav class="navbar navbar-expand-lg justify-content-between">
          <a class="navbar-brand text " href="#">
            Casino
          </a>
         
          <button
            onClick={handleClick}
            type="button"
            class="btn btn-outline-dark wallet"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              class="bi bi-wallet2"
              viewBox="0 0 16 16"
            >
              <path d="M12.136.326A1.5 1.5 0 0 1 14 1.78V3h.5A1.5 1.5 0 0 1 16 4.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 13.5v-9a1.5 1.5 0 0 1 1.432-1.499L12.136.326zM5.562 3H13V1.78a.5.5 0 0 0-.621-.484L5.562 3zM1.5 4a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z" />
            </svg>
            {(users.data) ? (" " + users?.data?.[0]?.credit) : " " }
            {/* {" " + users?.data?.[0]?.credit} */}
          </button>
          <div className="dropdown">
          <button
            type="button"
            class="btn btn-outline-dark dropdown-toggle"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              class="bi bi-person-circle"
              viewBox="0 0 16 16"
            >
              <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
              <path
                fill-rule="evenodd"
                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
              />
            </svg>
            {(users.data) ? (" " + users?.data?.[0]?.username) : " " }
            {/* // {" " + users?.data?.[0]?.username} */}
          </button>
          <ul class="dropdown-menu">
          <li>
            <a class="dropdown-item" onClick={handleClick} href="#">
              Logout
            </a>
          </li>
        </ul>
        </div>
        </nav>
        
        
      </div>
      <img
        src="https://casinorange.com/wp-content/uploads/2022/12/cms-banner-2000x500-n4486-v1.width-800.webp"
        height="300px"
        width="100%"
      />
      <Categories setSelectedCategory={setSelectedCategory} />

      {isLoading ? (
        <div className=" py-4 d-flex justify-content-center align-items-center">
          <div class="spinner-border text-light" role="status">
            <span class="visually-hidden"></span>
          </div>
        </div>
      ) : (
        <CasinoGames selectedCategory={selectedCategory} />
      )}
    </>
  );
};
