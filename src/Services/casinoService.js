import axios from "axios";

export const casinoService = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(" https://beta-admin.slotsandluck.com/accounts/get-casino-games")
      .then((response) => {
        console.log(response);
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
export const playGameService = (gameId,accountId) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        "https://beta-admin.slotsandluck.com/GSoft/",
        {
          params: {
            request: "startgame",
            nogs_game_id: gameId,
            nogs_lang: "en_US",
            nogs_mode: "real",
            account_id: accountId,
            country: "US",
            device_type: "desktop",
            is_test_account: false,
          },
        },
        {
          headers: {
            authorization: "JWT " + localStorage.getItem("token"),
          },
        }
      )
      .then((response) => {
        console.log(response);
        resolve(response?.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
