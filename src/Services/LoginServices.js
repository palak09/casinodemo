import axios from "axios";

export const LoginServices = ({username :uname,password:password}) => {
  return new Promise((resolve, reject)=>{
    axios
    .post(" https://beta-admin.slotsandluck.com/api/v0/users/login", {username: uname,password:password})
    .then((response) => {
      resolve(response.data)
    })
    .catch((error) => {
      reject(error)
    });
  })
};
export const UserServices = () => {
  return new Promise((resolve, reject)=>{
    axios
    .get(`https://beta-admin.slotsandluck.com/api/v0/players/?timestamp=${new Date().getTime()}`,	{
			headers: {
				'authorization' : 'JWT '+localStorage.getItem("token")
			}
		})
    .then((response) => {
      resolve(response)
    })
    .catch((error) => {
      reject(error)
    });
  })
};
