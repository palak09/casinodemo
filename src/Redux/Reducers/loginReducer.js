
const initialState = {
  uname: "",
  password: "",
  token: localStorage.getItem("token"),
  users :[]
};
const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN": {
      return {
        ...state,
        token: true
      };
    }
    case "LOGOUT": {
      return {
        ...state,
        token: false
      };
    }
    case "SETUSER": {
      return {
        ...state,
        users : action.payload
      };
    }
    default :
    return state
  }
};
export default loginReducer;
