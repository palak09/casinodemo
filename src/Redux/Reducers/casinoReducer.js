const initialState = {
    games : [],
    error : null,
    isLoading : false
}
const casinoReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_POST_START":
            return {
                ...state,
                isLoading:true
            }
            case "FETCH_POST_SUCCESS":
            return {
                ...state,
                isLoading:false,
                games:action.payload
            }
            case "FETCH_POST_FAIL":
            return {
                ...state,
                isLoading:false,
                games:action.payload
            }
            default:
                return state
    }
  };
  export default casinoReducer;