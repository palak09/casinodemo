import {combineReducers} from "redux"
import loginReducer from "./loginReducer"
import casinoReducer from "./casinoReducer"
const rootReducer = combineReducers({
    loginReducer, casinoReducer
})
export default rootReducer
