import { casinoService, playGameService } from "../../Services/casinoService";

const fetchStart = () => {
  return (dispatch, state) => {
    casinoService()
      .then((response) => {
        const games = response.data;
        dispatch(fetchSuccess(games));
      })
      .catch((error) => {
        dispatch(fetchFail(error.message));
      });
    dispatch({ type: "FETCH_POST_START" });
  };
};
const fetchSuccess = (games) => ({
  type: "FETCH_POST_SUCCESS",
  payload: games,
});
const fetchFail = (error) => ({
  type: "FETCH_POST_FAIL",
  payload: error,
});
export function fetchCasino() {
  return (dispatch) => {
    dispatch(fetchStart())
  };
}

export const playGame = (gameId ,accountId) => {
  return(dispatch,state) => {
    playGameService(gameId,accountId)
    .then((response) =>{
      console.log("Url",response.url.toString())
      window.location.href =  response.url.toString()
    })
    .catch(error => console.log(error))
  }
}