import { LoginServices,UserServices } from "../../Services/LoginServices";

export const login = (uname, password) => {
  return (dispatch, state) => {
    LoginServices( {username :uname, password :password} )
      .then((res) => {
        localStorage.setItem("token", res.auth_token);
        dispatch({ type: "LOGIN", payload: res.auth_token });
        dispatch (getUser())
        
      })
      .catch((rej) => {
        console.log(rej);
      });
  };
};
export const getUser = () => {
  return (dispatch, state) => {
    UserServices()
      .then((res) => {
        console.log(res)
        dispatch({type: "SETUSER", payload:res})
        
      })
      .catch((rej) => {
        console.log(rej);
      });
  };
};

 
export const logout = () => {
  localStorage.removeItem("token")
  return {
    type : "LOGOUT"
  }
}